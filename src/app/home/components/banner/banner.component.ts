import { Component, OnInit, AfterViewInit } from "@angular/core";
import Swiper from 'swiper';

@Component({
  selector: "app-banner",
  templateUrl: "./banner.component.html",
  styleUrls: ["./banner.component.scss"],
})
export class BannerComponent implements OnInit, AfterViewInit {
  mySwiper: Swiper;

  images: string[] = [
    "assets/images/banner-1.jpg",
    "assets/images/banner-2.jpg",
    "assets/images/banner-3.jpg",
  ];

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.mySwiper = new Swiper(".swiper-container", {
      observer: true,
      observeParents: true,
      parallax:true,
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      // If we need pagination
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    
      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      
    
      // And if we need scrollbar
      scrollbar: {
        el: '.swiper-scrollbar',
      },
    });
  }
}
